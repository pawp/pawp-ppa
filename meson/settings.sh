COMPR="gz"
RELEASE_VERSION="0.52.0"
INC=2
DISTROS="eoan"
URL="https://github.com/mesonbuild/meson/releases/download/${RELEASE_VERSION}/"
